# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

cmake_minimum_required(VERSION 3.13...3.14.2)

project(CppOtl CXX)

add_library(${PROJECT_NAME} INTERFACE)

target_include_directories(${PROJECT_NAME} INTERFACE include)

set(CppOtl.Include.dir ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(CppOtl.Include.files
    include/otn/access.hpp
    include/otn/all.hpp
    include/otn/conform.hpp
    include/otn/cpp_lang.hpp
    include/otn/default.hpp
    include/otn/interaction/safe_raw.hpp
    include/otn/interaction/safe_slim.hpp
    include/otn/interaction/slim_raw.hpp
    include/otn/raw.hpp
    include/otn/safe.hpp
    include/otn/slim.hpp
    include/otn/std_smart.hpp
    include/otn/utility.hpp
    include/otn/v1/access/detail/access.hpp
    include/otn/v1/access/detail/assembly.hpp
    include/otn/v1/access/detail/gain.hpp
    include/otn/v1/access/functions.hpp
    include/otn/v1/base/configs.hpp
    include/otn/v1/base/detail/assembly.hpp
    include/otn/v1/base/detail/comparison.hpp
    include/otn/v1/base/detail/concepts.hpp
    include/otn/v1/base/detail/hash.hpp
    include/otn/v1/base/detail/pointer_traits.hpp
    include/otn/v1/base/detail/referrer.hpp
    include/otn/v1/base/detail/token.hpp
    include/otn/v1/base/detail/traits.hpp
    include/otn/v1/base/factory.hpp
    include/otn/v1/base/rules.hpp
    include/otn/v1/base/summaries.hpp
    include/otn/v1/base/tags.hpp
    include/otn/v1/base/traits.hpp
    include/otn/v1/basis/names.hpp
    include/otn/v1/basis/traits.hpp
    include/otn/v1/collection/detail/iterator.hpp
    include/otn/v1/conform/token.hpp
    include/otn/v1/conversion/operations.hpp
    include/otn/v1/conversion/properties.hpp
    include/otn/v1/conversion/rules.hpp
    include/otn/v1/cpp_lang/basis.hpp
    include/otn/v1/cpp_lang/detail/assembly.hpp
    include/otn/v1/cpp_lang/detail/comparison.hpp
    include/otn/v1/cpp_lang/detail/concrete.hpp
    include/otn/v1/cpp_lang/detail/conversion.hpp
    include/otn/v1/cpp_lang/detail/generic.hpp
    include/otn/v1/cpp_lang/detail/hash.hpp
    include/otn/v1/cpp_lang/detail/pointer_traits.hpp
    include/otn/v1/cpp_lang/detail/swap.hpp
    include/otn/v1/cpp_lang/detail/traits.hpp
    include/otn/v1/cpp_lang/origin.hpp
    include/otn/v1/cpp_lang/unified_optional.hpp
    include/otn/v1/cpp_lang/unified_unknown.hpp
    include/otn/v1/default/import.hpp
    include/otn/v1/deleter/names.hpp
    include/otn/v1/deleter/traits.hpp
    include/otn/v1/element/summaries.hpp
    include/otn/v1/element/traits.hpp
    include/otn/v1/generic/bind.hpp
    include/otn/v1/generic/token.hpp
    include/otn/v1/interaction/base_cpp_lang.hpp
    include/otn/v1/interaction/base_cpp_lang/detail/comparison.hpp
    include/otn/v1/interaction/base_std_smart.hpp
    include/otn/v1/interaction/base_std_smart/detail/comparison.hpp
    include/otn/v1/interaction/raw_cpp_lang.hpp
    include/otn/v1/interaction/raw_cpp_lang/detail/conversion.hpp
    include/otn/v1/interaction/raw_cpp_lang/detail/referrer.hpp
    include/otn/v1/interaction/raw_std_smart.hpp
    include/otn/v1/interaction/raw_std_smart/detail/conversion.hpp
    include/otn/v1/interaction/safe_cpp_lang.hpp
    include/otn/v1/interaction/safe_cpp_lang/detail/conversion.hpp
    include/otn/v1/interaction/safe_raw.hpp
    include/otn/v1/interaction/safe_raw/detail/conversion.hpp
    include/otn/v1/interaction/safe_slim.hpp
    include/otn/v1/interaction/safe_slim/detail/conversion.hpp
    include/otn/v1/interaction/safe_std_smart.hpp
    include/otn/v1/interaction/safe_std_smart/detail/conversion.hpp
    include/otn/v1/interaction/safe_std_smart/detail/referrer.hpp
    include/otn/v1/interaction/slim_cpp_lang.hpp
    include/otn/v1/interaction/slim_cpp_lang/detail/conversion.hpp
    include/otn/v1/interaction/slim_raw.hpp
    include/otn/v1/interaction/slim_raw/detail/conversion.hpp
    include/otn/v1/interaction/slim_std_smart.hpp
    include/otn/v1/interaction/slim_std_smart/detail/conversion.hpp
    include/otn/v1/interaction/slim_std_smart/detail/generic.hpp
    include/otn/v1/interaction/slim_std_smart/detail/referrer.hpp
    include/otn/v1/interaction/std_smart_cpp_lang.hpp
    include/otn/v1/interaction/std_smart_cpp_lang/detail/base.hpp
    include/otn/v1/interaction/std_smart_cpp_lang/detail/comparison.hpp
    include/otn/v1/interaction/std_smart_cpp_lang/detail/conversion.hpp
    include/otn/v1/interaction/std_smart_cpp_lang/detail/referrer.hpp
    include/otn/v1/lifetime/detector.hpp
    include/otn/v1/lifetime/names.hpp
    include/otn/v1/lifetime/rules.hpp
    include/otn/v1/lifetime/traits.hpp
    include/otn/v1/multiplicity/names.hpp
    include/otn/v1/multiplicity/traits.hpp
    include/otn/v1/owner_base/names.hpp
    include/otn/v1/owner_base/rules.hpp
    include/otn/v1/owner_base/traits.hpp
    include/otn/v1/ownership/names.hpp
    include/otn/v1/ownership/traits.hpp
    include/otn/v1/proxy/detail/assembly.hpp
    include/otn/v1/proxy/detail/list.hpp
    include/otn/v1/proxy/detail/list_iterator.hpp
    include/otn/v1/proxy/detail/range_access.hpp
    include/otn/v1/proxy/factory.hpp
    include/otn/v1/raw/basis.hpp
    include/otn/v1/raw/detail/assembly.hpp
    include/otn/v1/raw/detail/concrete.hpp
    include/otn/v1/raw/detail/conversion.hpp
    include/otn/v1/raw/detail/generic.hpp
    include/otn/v1/raw/detail/hash.hpp
    include/otn/v1/raw/detail/pointer_traits.hpp
    include/otn/v1/raw/detail/range_access.hpp
    include/otn/v1/raw/detail/swap.hpp
    include/otn/v1/raw/detail/token.hpp
    include/otn/v1/raw/spec.hpp
    include/otn/v1/referrer/adaptor.hpp
    include/otn/v1/referrer/factory.hpp
    include/otn/v1/referrer/rules.hpp
    include/otn/v1/referrer/summaries.hpp
    include/otn/v1/referrer/tags.hpp
    include/otn/v1/referrer/traits.hpp
    include/otn/v1/safe/basis.hpp
    include/otn/v1/safe/detail/assembly.hpp
    include/otn/v1/safe/detail/base.hpp
    include/otn/v1/safe/detail/concrete.hpp
    include/otn/v1/safe/detail/conform.hpp
    include/otn/v1/safe/detail/conversion.hpp
    include/otn/v1/safe/detail/factory.hpp
    include/otn/v1/safe/detail/generic.hpp
    include/otn/v1/safe/detail/hash.hpp
    include/otn/v1/safe/detail/pointer_traits.hpp
    include/otn/v1/safe/detail/proxy.hpp
    include/otn/v1/safe/detail/range_access.hpp
    include/otn/v1/safe/detail/referrer.hpp
    include/otn/v1/safe/detail/swap.hpp
    include/otn/v1/safe/detail/token.hpp
    include/otn/v1/safe/spec.hpp
    include/otn/v1/slim/basis.hpp
    include/otn/v1/slim/detail/assembly.hpp
    include/otn/v1/slim/detail/base.hpp
    include/otn/v1/slim/detail/concrete.hpp
    include/otn/v1/slim/detail/conversion.hpp
    include/otn/v1/slim/detail/factory.hpp
    include/otn/v1/slim/detail/generic.hpp
    include/otn/v1/slim/detail/hash.hpp
    include/otn/v1/slim/detail/pointer_traits.hpp
    include/otn/v1/slim/detail/range_access.hpp
    include/otn/v1/slim/detail/swap.hpp
    include/otn/v1/slim/detail/token.hpp
    include/otn/v1/slim/spec.hpp
    include/otn/v1/spec/brief.hpp
    include/otn/v1/spec/detail/assembly.hpp
    include/otn/v1/spec/detail/editor.hpp
    include/otn/v1/spec/editor.hpp
    include/otn/v1/spec/traits.hpp
    include/otn/v1/std_smart/basis.hpp
    include/otn/v1/std_smart/detail/assembly.hpp
    include/otn/v1/std_smart/detail/concrete.hpp
    include/otn/v1/std_smart/detail/conversion.hpp
    include/otn/v1/std_smart/detail/generic.hpp
    include/otn/v1/std_smart/detail/owner_base.hpp
    include/otn/v1/std_smart/detail/pointer_traits.hpp
    include/otn/v1/std_smart/detail/proxy.hpp
    include/otn/v1/std_smart/detail/referrer.hpp
    include/otn/v1/std_smart/detail/traits.hpp
    include/otn/v1/std_smart/origin.hpp
    include/otn/v1/support/assert.hpp
    include/otn/v1/support/concept.hpp
    include/otn/v1/support/noexcept.hpp
    include/otn/v1/support/pointer_traits.hpp
    include/otn/v1/support/to_value.hpp
    include/otn/v1/support/type_traits.hpp
    include/otn/v1/support/utilize.hpp
    include/otn/v1/traits/derivative.hpp
    include/otn/v1/utility/object_factory.hpp
    include/otn/v1/utility/unique_carrier.hpp
)

source_group(TREE ${CppOtl.Include.dir} FILES ${CppOtl.Include.files})
